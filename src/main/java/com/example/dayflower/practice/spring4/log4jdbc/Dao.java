package com.example.dayflower.practice.spring4.log4jdbc;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * This annotation is only used to indicate that target interface is a MyBatis mapper (for filtering purpose)
 */

@Target(ElementType.TYPE)
public @interface Dao {
}
