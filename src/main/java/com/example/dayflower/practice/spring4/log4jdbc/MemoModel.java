package com.example.dayflower.practice.spring4.log4jdbc;

import lombok.Data;

@Data
public class MemoModel {
	private int id;
	private String title;
	private String content;
}
