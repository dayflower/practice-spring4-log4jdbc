## 目的

* JDBC 経由で発行されるクエリを log4jdbc(-log4j2) でトレースする。

## 苦労したところ

* まだ出力を調整していないので苦労していない。
* log4jdbc(-log4j2) を間に挟むために、 data source を Spring の jdbc:embedded-database から DriverManagerDataSource に変更する必要があった。
* じっさいのところ、 Spring 自身が DEBUG にクエリログを吐いているようなので、今回のアプローチは不要な気がする。
* いまのところ log4jdbc-log4j2 が一番新しいプロジェクトっぽい。名前からうける印象と異なり、 Slf4j にも対応している。単純に enhanced 版。
